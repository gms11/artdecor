﻿using ArtDecor.Models;
using ArtDecor.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtDecor.Services
{
    public class QuotationService
    {
        private QuotationRepository _QuotationRepository;
        public QuotationService()
        {
            _QuotationRepository = new QuotationRepository();
        }

        public void  Save(IList<Quotation> quaotations)
        {

             _QuotationRepository.Save(quaotations);
        }
    }
}
