﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtDecor.Models
{
   public class Quotation
    {
        public int QuotationId { get; set; }
        public string QuotationDate { get; set; }
        public string QuotationName { get; set; }
        public string CustomerName { get; set; }
        public int? IsProforma { get; set; }

      public  IList<QuotationsPrices> quotationPrices { get; set; }

    }
}
