﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace ArtDecor.Application.Filters
{
    public class ExceptionFilterAttribute : System.Web.Http.Filters.ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            Logger.Log.Error(context.Exception.Message, context.Exception);
            context.Response = new HttpResponseMessage(HttpStatusCode.BadRequest);

        }
    }
}