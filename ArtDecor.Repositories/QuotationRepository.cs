﻿using ArtDecor.Models;
using ArtDecor.Persistance;
using ArtDecor.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtDecor.Repositories
{
    public class QuotationRepository
    {
        private readonly string QUOATION_INSERT_STATEMENT= "INSERT INTO quotations " +
"(QuotationDate,QuotationName,CustomerName,IsProforma) " +
"VALUES ('{0}','{1}','{2}',{3}); ";

        private readonly string QUOATION_LINE_INSERT_STATEMENT = "INSERT INTO quotationsprices " +
" (quotationId,ProductId,ProductPrice,Remarks) " +
" VALUES({0},{1},{2},{3})";

        public IList<Quotation> List()
        {
            var dt = Persistance.DbMySQL.fill_DataTable("select * from quotations", null);
            IList<Quotation> quotions = dt.AsEnumerable().Select(m => new Quotation()
            {

                QuotationId = int.Parse(m["QuotationId"].ToString()),
                QuotationDate = CommonUtils.GetStringDb(m["QuotationDate"]),
                QuotationName = CommonUtils.GetStringDb(m["QuotationName"]),
                CustomerName = CommonUtils.GetStringDb(m["CustomerName"]),
                IsProforma = CommonUtils.GetIntDb(m["IsProforma"]),


            }).ToList();

            return quotions;
            // dt = Persistance.DbMySQL.fill_DataTable("select * from quotationsprices", null);
        }



        public void Save(IList<Quotation> quotations)
        {

            foreach (var p in quotations)
            {
                DbMySQL.Action_Query(string.Format(QUOATION_INSERT_STATEMENT, p.QuotationDate, p.QuotationName, p.CustomerName,
p.IsProforma.HasValue ? p.IsProforma.Value.ToString() : "null"));

                var quotationId = DbMySQL.Get_Val_from_Query("select max(QuotationId) from quotations");
                var commissionAgent = DbMySQL.Get_Val_from_Query($"select agentCommission from customers where customerName =N'{ p.CustomerName}'");

                decimal commisonAgent = 0;
                if (!string.IsNullOrEmpty(CommonUtils.GetStringDb(commissionAgent)))
                {
                    commisonAgent = decimal.Parse(commissionAgent) / 100 + 1;
                }

                foreach (var m in p.quotationPrices)
                {
                    decimal listPrice = 0;
                    decimal.TryParse(DbMySQL.Get_Val_from_Query($"select listprice from products where productid ={m.ProductId}"), out listPrice);
                    DbMySQL.Action_Query(string.Format(QUOATION_LINE_INSERT_STATEMENT, quotationId,
                        m.ProductId.HasValue ? m.ProductId.Value : (int?)null,
                     (commisonAgent) * listPrice,
                        string.IsNullOrEmpty(m.Remarks) ? "null" : m.Remarks));
                }
            }


            // dt = Persistance.DbMySQL.fill_DataTable("select * from quotationsprices", null);


        }
    }
}
