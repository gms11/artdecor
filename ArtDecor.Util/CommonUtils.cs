﻿using System;



namespace ArtDecor.Util
{
    public static class CommonUtils
    {


      
        public static int? GetIntDb(object value)
        {
            return (value == DBNull.Value) ? (int?)null : int.Parse(value.ToString());
        }


        public static string GetStringDb(object value)
        {
            return (value == DBNull.Value) ? null : value.ToString();
        }

        public static decimal? GetDecimalDb(object value)
        {
            return (value == DBNull.Value) ? (decimal?)null : decimal.Parse(value.ToString());
        }
    }
}
