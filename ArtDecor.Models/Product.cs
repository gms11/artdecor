﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtDecor.Models
{
    public class Product
    {

        public int ProductId { get; set; }
        public string ProductCode { get; set; }
      //  public string SupplierProductCode { get; set; }
       // public int? CategoryId { get; set; }
        //public string CategorySymbol { get; set; }
        //public int? SubCategoryId { get; set; }
        //public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        //public string CatalogNumber { get; set; }
        //public int? SupplierId { get; set; }

        public decimal? MinimumForOrder { get; set; }
        public string ProductSize { get; set; }
        //public string Packing { get; set; }
        //public int? AmountOfPieces { get; set; }
        //public decimal? PackageLength { get; set; }
        //public decimal? PackageWidth { get; set; }
        //public decimal? PackageHeight { get; set; }
        //public int? MasterCartonCapacity { get; set; }
        //public int? InnerCartonCapacity { get; set; }
        //public string HSCode { get; set; }
        //public decimal? PackageGrossWeight { get; set; }
        //public decimal? PackageNetWeight { get; set; }
        //public int? IsMixedPackage { get; set; }
        //public string ShippingPort { get; set; }
        //public decimal? StandartCost { get; set; }
        public decimal? ListPrice { get; set; }
        //public int? Currency { get; set; }
        //public string ProductImagePath { get; set; }
        //public string Comments { get; set; }

        public IList<ProductImage>  ProductImages {get; set;}
    }
}
