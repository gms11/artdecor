﻿using ArtDecor.Models;
using ArtDecor.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtDecor.Repositories
{
    public class ProductRepository
    {

        public IList<Product> List()
        {
            var dtProducts = Persistance.DbMySQL.fill_DataTable("select * from products", null);


            var products= dtProducts.AsEnumerable().Select(m => new Product()
            {
                ProductId = int.Parse(m["ProductId"].ToString()),
                ProductCode = CommonUtils.GetStringDb(m["ProductCode"]),
               // SupplierProductCode = CommonUtils.GetStringDb(m["SupplierProductCode"]),
                //CategoryId = CommonUtils.GetIntDb(m["CategoryId"]),
                //CategorySymbol = CommonUtils.GetStringDb(m["CategorySymbol"]),
               // SubCategoryId = CommonUtils.GetIntDb(m["SubCategoryId"]),
               // ProductName = CommonUtils.GetStringDb(m["ProductName"]),
                ProductDescription = CommonUtils.GetStringDb(m["ProductDescription"]),
                //CatalogNumber = CommonUtils.GetStringDb(m["CatalogNumber"]),
                //SupplierId = CommonUtils.GetIntDb(m["SupplierId"]),
                MinimumForOrder = CommonUtils.GetDecimalDb(m["MinimumForOrder"]),
                ProductSize = CommonUtils.GetStringDb(m["ProductSize"]),
                //Packing = CommonUtils.GetStringDb(m["Packing"]),
                //AmountOfPieces = CommonUtils.GetIntDb(m["AmountOfPieces"]),
                //PackageLength = CommonUtils.GetDecimalDb(m["PackageLength"]),
                //PackageWidth = CommonUtils.GetDecimalDb(m["PackageWidth"]),
                //PackageHeight = CommonUtils.GetDecimalDb(m["PackageHeight"]),
                //MasterCartonCapacity = CommonUtils.GetIntDb(m["MasterCartonCapacity"]),
                //InnerCartonCapacity = CommonUtils.GetIntDb(m["InnerCartonCapacity"]),
                //HSCode = CommonUtils.GetStringDb(m["HSCode"]),
                //PackageGrossWeight = CommonUtils.GetDecimalDb(m["PackageGrossWeight"]),
                //PackageNetWeight = CommonUtils.GetDecimalDb(m["PackageNetWeight"]),
                //IsMixedPackage = CommonUtils.GetIntDb(m["IsMixedPackage"]),
                //ShippingPort = CommonUtils.GetStringDb(m["ShippingPort"]),
                //StandartCost = CommonUtils.GetDecimalDb(m["StandartCost"]),
                ListPrice = CommonUtils.GetDecimalDb(m["ListPrice"]),
                //Currency = CommonUtils.GetIntDb(m["Currency"]),
                //ProductImagePath = CommonUtils.GetStringDb(m["ProductImagePath"]),
                //Comments = CommonUtils.GetStringDb(m["Comments"]),
            }).ToList();



          var productImages=  Persistance.DbMySQL.fill_DataTable("select * from productsimages where ismain = 1", null).AsEnumerable().
               Select(m => new ProductImage() {
                   ImagePath = CommonUtils.GetStringDb(m["ImagePath"]),
                   ProductId= CommonUtils.GetIntDb(m["ProductId"]),

               });

            foreach (var p in products)
            {
                p.ProductImages = productImages.Where(m => m.ProductId == p.ProductId).ToList();
            }

            return products;

        }
    }
}
