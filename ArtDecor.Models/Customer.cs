﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtDecor.Models
{
    public class Customer
    {

    //    public int CustomerId { get; set; }
        public string CustomerName { get; set; }
      //  public string RegistrationNumber { get; set; }
        public string Contact { get; set; }
    //    public string ContactRole { get; set; }
        public string ContactMobile { get; set; }

  //      public string Country { get; set; }

//        public string City { get; set; }

        //public string Address { get; set; }


        //public string ZipCode { get; set; }

        //public string Phone { get; set; }


      //  public string Mobile { get; set; }

        public string Email { get; set; }

      //  public string Fax { get; set; }

      //  public string DealerNumber { get; set; }

     //   public string NameInInvoice { get; set; }

      //  public decimal? Commission { get; set; }
        public int? AgentCommission { get; set; }

    }
}
