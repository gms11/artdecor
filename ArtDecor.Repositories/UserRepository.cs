﻿using ArtDecor.Models;
using ArtDecor.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtDecor.Repositories
{
    public class UserRepository
    {

        public IList<User> List()
        {
            var dt = Persistance.DbMySQL.fill_DataTable("select * from users", null);


            return dt.AsEnumerable().Select(m => new User()
            {
                UserId = int.Parse(m["UserId"].ToString()),
                UserName = CommonUtils.GetStringDb(m["UserName"]),
             //   Role = CommonUtils.GetStringDb(m["Role"]),
              //  Authorization = CommonUtils.GetIntDb(m["Authorization"]),
               // Email = CommonUtils.GetStringDb(m["Email"]),
                Password = CommonUtils.GetStringDb(m["Password"]),
               
            }).ToList();
        }
    }
}
