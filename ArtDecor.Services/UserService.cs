﻿using ArtDecor.Models;
using ArtDecor.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtDecor.Services
{
    public class UserService
    {
        private UserRepository _UserRepository;
        public UserService()
        {
            _UserRepository = new UserRepository();
        }

        public IList<User> List()
        {

            return _UserRepository.List();
        }
    }
}
