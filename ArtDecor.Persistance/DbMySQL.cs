﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;

namespace ArtDecor.Persistance
{
  public  class DbMySQL 
    { 
        public static MySqlConnection Conn;
        public static MySqlCommand Cmd;
   
        public static MySqlDataAdapter adp;
        public static string Connection;

        static DbMySQL()
        {
            Connection = ConfigurationManager.ConnectionStrings["ArtDecor"].ConnectionString;
        }
        public static bool OpenConnection()
        {
            Conn = new MySqlConnection(Connection);
            try
            {
                Conn.Open();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static DataSet fill_Dataset(string SQL, string Table)
        {
            using (var con = new MySqlConnection(Connection))
            {
                con.Open();
              var  dsView = new DataSet();
                using (var adp1 = new MySqlDataAdapter(SQL, con))
                {
                    if (string.IsNullOrEmpty(Table))
                        Table = "table";
                    adp1.Fill(dsView, Table);

                }

                con.Close();
                return dsView;
            }
        }
        public static DataTable fill_DataTable(string sql, string table)
        {
            try
            {
                using (var con = new MySqlConnection(Connection))
                {
                    con.Open();
                    var dsView1 = new DataSet();
                    using (var adapter1 = new MySqlDataAdapter(sql, con))
                    {
                        if (string.IsNullOrEmpty(table))
                            table = "table";
                        adapter1.Fill(dsView1, table);
                    }
                    return dsView1.Tables[table];
                }
            }
            catch (Exception)
            {

                throw;
            }




        }

        public static string Get_Val_from_Query(string sql)
        {
            string res;
            using (var con = new MySqlConnection(Connection))
            {

                using (MySqlCommand Command = new MySqlCommand(sql, con))

                {
                    con.Open();
                    Command.ExecuteNonQuery();
                    try
                    {
                        res = Command.ExecuteScalar().ToString();
                    }
                    catch
                    {
                        res = "0";
                    }
                    return res;
                }
            }


        }



        public static void Action_Query(string sql)
        {
            try
            {
                using (var con = new MySqlConnection(Connection))
                {
                    con.Open();
                    using (var sqlCommand = new MySqlCommand(sql, con))
                    {
                        sqlCommand.ExecuteNonQuery();

                    }


                }

                
            }
            catch(Exception ex)
            {
                throw;
            }
            finally
            {

            }

        }




      



     
      

     



    }
}
