﻿using ArtDecor.Models;
using ArtDecor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ArtDecor.Application.Controllers
{
    [RoutePrefix("api/sync")]
    public class SyncController : ApiController
    {

        [HttpGet]
        [Route("products")]
        public IList<Product> GetProducts()
        {
            return new ProductService().List().ToList();
        }



        [HttpGet]
        [Route("customers")]
        public IList<Customer> GetCustomers()
        {
            return new CustomerService().List();
        }


        [HttpGet]
        [Route("users")]
        public IList<User> GetUsers()
        {
            return new UserService().List();
        }


        [HttpPost]
        [Route("quotations")]
        public IHttpActionResult SaveQuotations(IList<Quotation> quotations)
        {
             new QuotationService().Save(quotations);
            return Ok();
        }



        [HttpPost]
        [Route("customers")]
        public IHttpActionResult SaveCustomers(IList<Customer> customers)
        {
            new CustomerService().Save(customers);

            return Ok();
        }
    }
}
