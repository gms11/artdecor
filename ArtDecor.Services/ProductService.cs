﻿using ArtDecor.Models;
using ArtDecor.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtDecor.Services
{
    public class ProductService
    {
        private ProductRepository _ProductRepository;
        public ProductService()
        {
            _ProductRepository = new ProductRepository();
        }

        public IList<Product> List()
        {

            return _ProductRepository.List();
        }
    }
}
