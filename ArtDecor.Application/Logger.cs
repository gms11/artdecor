﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using log4net;
using log4net.Config;

namespace ArtDecor.Application
{
    //remakrks adi
    public static class Logger
    {
        static Logger()  
        {
            XmlConfigurator.Configure(new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "log4net.config"));
        }
         
        public static readonly ILog Log = LogManager.GetLogger("ArtDecor");
    }
}
