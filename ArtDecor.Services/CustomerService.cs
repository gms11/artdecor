﻿using ArtDecor.Models;
using ArtDecor.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtDecor.Services
{
    public class CustomerService
    {

       
        private CustomerRepository _CustomerRepository;
        public CustomerService()
        {
            _CustomerRepository = new CustomerRepository();
        }

        public IList<Customer> List()
        {

            return _CustomerRepository.List();
        }

        public void Save(IList<Customer> customers)
        {
            _CustomerRepository.Save(customers);
        }
    }
    
}
