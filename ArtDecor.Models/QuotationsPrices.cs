﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtDecor.Models
{
    public class QuotationsPrices
    {
      //  public int RowId { get; set; }
        public int? QuotationId { get; set; }
        public int? ProductId { get; set; }
       public decimal? ProductPrice { get; set; }
        public string Remarks { get; set; }
    }
}
