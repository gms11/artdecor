﻿using ArtDecor.Models;
using ArtDecor.Persistance;
using ArtDecor.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtDecor.Repositories
{
    public class CustomerRepository
    {
        private readonly string CUSTOMER_INSERT_STATEMENT = "INSERT INTO customers " +
"(CustomerName,Contact,ContactMobile,Email,AgentCommission) " +
"VALUES (N'{0}',N'{1}','{2}','{3}',{4}); ";


        private readonly string CUSTOMER_UPDATE_STATEMENT = $"UPDATE customers " +
"Set CustomerName =N'{0}',Contact=N'{1}',ContactMobile={2},Email={3},AgentCommission={4} Where CustomerName=N'{0}' ";

        public IList<Customer> List()
        {
            var dt = Persistance.DbMySQL.fill_DataTable("select * from customers", null);


            return dt.AsEnumerable().Select(m => new Customer()
            {
                 
              //  CustomerId = int.Parse(m["CustomerId"].ToString()),
                CustomerName = CommonUtils.GetStringDb(m["CustomerName"]),
               // RegistrationNumber = CommonUtils.GetStringDb(m["RegistrationNumber"]),
                Contact = CommonUtils.GetStringDb(m["Contact"]),
               // ContactRole = CommonUtils.GetStringDb(m["ContactRole"]),
                ContactMobile = CommonUtils.GetStringDb(m["ContactMobile"]),
               // Country = CommonUtils.GetStringDb(m["Country"]),
                //City = CommonUtils.GetStringDb(m["City"]),
               // Address = CommonUtils.GetStringDb(m["Address"]),
               // ZipCode = CommonUtils.GetStringDb(m["ZipCode"]),
                //Phone = CommonUtils.GetStringDb(m["Phone"]),
               // Mobile = CommonUtils.GetStringDb(m["Mobile"]),
               // Email = CommonUtils.GetStringDb(m["Email"]),
                //Fax = CommonUtils.GetStringDb(m["Fax"]),
               // DealerNumber = CommonUtils.GetStringDb(m["DealerNumber"]),
               // NameInInvoice = CommonUtils.GetStringDb(m["NameInInvoice"]),
               // Commission = CommonUtils.GetDecimalDb(m["Commission"]),
                AgentCommission = CommonUtils.GetIntDb(m["AgentCommission"])


            }).ToList();
        }

        public void Save(IList<Customer> customers)
        {


            foreach (var p in customers)
            {
                if (DbMySQL.Get_Val_from_Query($"select customerid from customers where CustomerName=N'{p.CustomerName}'") == "0")
                {
                    DbMySQL.Action_Query(string.Format(CUSTOMER_INSERT_STATEMENT, p.CustomerName, p.Contact,
                                           p.ContactMobile, p.Email ?? "null", p.AgentCommission.HasValue ? p.AgentCommission.Value.ToString() : "null"));

                }
                else
                {
                    if(!p.AgentCommission.HasValue)
                   
                        p.AgentCommission = 0;
                    }
                    DbMySQL.Action_Query(string.Format(CUSTOMER_UPDATE_STATEMENT,p.CustomerName,p.Contact,p.ContactMobile,p.Email??"null",p.AgentCommission));

                }


            }

        
    }
}
